**项目说明** 
- 艺特微课 - 是把互联网与传统教育行业结合，汇聚了众多知名教育机构的面授和网络培训课程资源。
致力于打造一个线上线下相结合的教育平台，为学习者提供最简单快速的选课模式、最优惠的课程；
同时也帮助教育培训机构以低成本、高效的网络营销方式，完成招生工作。
<br>
<br>

**平台功能介绍** 
 - 视频在线点播
 - 课程评论/视频评论
 - 手机号注册/登录(短信登录)/密码找回/手机号绑定
 - 集成微信支付
 - 邀请/一级返现/银行卡提现
 - 七牛云存储/阿里云oss存储/腾讯云
 - 后台多角色多权限管理
 - 支持线下线上课程（图文/视频/音频）发布
 - SEO优化
 - 支持第三方商户入驻

<br> 

**目录说明** 
```
ert-wike
├─admin-pc-ui  管理系统PC端
│
├─admin-h5-ui  管理系统H5端
│ 
├─java-server  JAVA服务端
│ 
├─user-h5-ui   用户H5端

```
<br> 

 **后端部署**
 - 通过git下载源码
 - 创建数据库ert-wike，数据库编码为utf8mb4
 - 执行db/mysql.sql文件，初始化数据
 - 修改application-dev.yml，更新MySQL及Redis账号和密码
 - Eclipse、IDEA运行WikeApplication.java，则可启动项目
 - 项目后端地址：http://127.0.0.1:8080/

<br> 

 **前端部署**
 - admin-pc-ui
   - 通过git下载源码
   - 安装node环境
   - 进入admin-pc-ui根目录执行npm run dev/cnpm run dev 安装依赖
   - 修改./static/config/index.js 目录文件中 window.SITE_CONFIG['baseUrl'] = '本地api接口请求地址'
   - 执行npm run dev 
 - user-h5-ui
   - 安装nginx
   - 将文件夹放置到nginx的root目录下
   - 修改./mobile/assets/js/method.js 目录文件中 serverURL ='/wx'
   - 配置nginx转发并启动
   ```
   location /wx {
        proxy_pass http://wike.com/; 
        proxy_connect_timeout 600s;
        proxy_read_timeout 600s;
        proxy_send_timeout 600s;
        proxy_redirect default;  
       }
       
   upstream wike.com {  
        server    172.18.136.151:8080 weight=2 fail_timeout=2s max_fails=1;
        #server   172.18.136.152:8080 weight=1 fail_timeout=2s max_fails=1;
       
       }    
   ```
   - admin-h5-ui
      - 同上 
 <br>

 **项目演示**
- 管理端演示地址：https://demo.uboto.cn/wike
- 用户端演示地址：https://www.uboto.cn/wike/mobile/index.html
- 运营账户：admin/wike
- 商户账户：13688888888/111111
- 切换帐号之后最好刷新浏览器
<br> 

**系统预览：**
![输入图片说明](http://img.uboto.cn/upload/20200923/135e301525a044d091437bf30ceb4928.png "在这里输入图片标题")
<br> 
<br>
![输入图片说明](http://img.uboto.cn/upload/20200923/9c528d84b6b241ad933d2c08f1ef0299.png "在这里输入图片标题")
<br> <br> 

**用户H5端预览：**
<br>
![输入图片说明](http://img.uboto.cn/upload/20200923/1e1881d4e1d84737b9d760f38fa3bcea.png "在这里输入图片标题")
![输入图片说明](http://img.uboto.cn/upload/20200923/79b822d547744ab8bbbeda08ef426879.png "在这里输入图片标题")
![输入图片说明](http://img.uboto.cn/upload/20200923/a93ca7cfee4240349c212769e53d5de3.png "在这里输入图片标题")
![输入图片说明](http://img.uboto.cn/upload/20200923/6654b460d77f4e7091c626796385ea9a.png "在这里输入图片标题")

<br>
